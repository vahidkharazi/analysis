#!/usr/bin/python
import turtle
import time
import random
#------------------------------
#GLOBAL VARIABLE
#------------------------------
scale=20.0
coefficients=[3,0,2,-10]				#coefficients of equvaliation
#------------------------------
#GLOBAL VARIABLE
#------------------------------
page_title="salam"
bg_color="#E0EEE0"
#------------------------------

for_delay = turtle.Turtle()
screen = for_delay.getscreen()
screen.delay(0)

class Equation:
	def __init__(self,*parameters):
		if len(parameters) == 1:
			self.coefficients = parameters[0]
			self.daraje = len(parameters[0]) - 1
		elif len(parameters) > 1:
			self.coefficients = list()
			for item in parameters:
				self.coefficients.append(item)
			self.daraje = len(parameters) - 1

#	def __init__(self,*parameters):
#		self.coefficients = list()
#		for item in parameters:
#			self.coefficients.append(item)
#		self.daraje = len(parameters) - 1

	def __init__(self,parameters):
		self.coefficients = parameters
		self.daraje = len(parameters) - 1
		

		self.bisection_steps=[]#bisection steps [start, end]
		self.dislocation_steps=[]#dislocation steps [start, end]
		self.newton_steps=[]
		self.tendinous_steps=[]		
	def __str__(self):
		res = ""
		for i in range(self.daraje):
			if self.coefficients[i] == 0:
				continue
			if self.coefficients[i] < 0:
				res = res + "%s*x^%d" %(str(self.coefficients[i]),self.daraje-i)
			else:
				res = res + "+%s*x^%d" %(str(self.coefficients[i]),self.daraje-i)
		if self.coefficients[-1] < 0:
			res = res + "%s" %str(self.coefficients[-1])
		else:
			res = res + "+%s" %str(self.coefficients[-1])
		return res

	def delay(self):
		time.sleep(3)

	def derive(self):
		temp = list()
		for i in range(self.daraje):
			temp.append(self.coefficients[i]*(self.daraje - i))
		return Equation(temp)

	def eval(self,x):
		res = 0.0
		for i in range(self.daraje + 1):
			res += self.coefficients[-i-1]*x**i
		return res

	def draw(self,curve_range=(-10,10),step=0.01,width=1):
		""" draw equation in scence"""		
		curve_turtle=turtle.Turtle()
		curve_turtle.penup()
		curve_turtle.speed(0)
		curve_turtle.width(width)
		curve_turtle.setpos(curve_range[0]*scale,self.eval(curve_range[0])*scale)	

		a=int(curve_range[0]*scale)
		b=int(curve_range[1]*scale)

		i = curve_range[0]

		curve_turtle.pendown()
		while i<curve_range[1]:
			print self.eval(i)
			curve_turtle.goto(i*scale , self.eval(i)*scale)
			i+=step
		del(curve_turtle)


	def draw_line(self,start,end,width=1):
		'''
			draw a line from start to end
			start is position of start of line(tuple or list) and end is position of end of line(tuple or list);		
		'''
		line_turtle=turtle.Turtle()
		line_turtle.penup()
		line_turtle.width(width)
		line_turtle.setpos(start[0]*scale,start[1]*scale)	
		line_turtle.pendown()
		line_turtle.goto(end[0]*scale,end[1]*scale)
		del(line_turtle)
		
	
	def find_dis(self):
		Start ,End = random.random(),random.random()
		Flag = 1
		while not self.eval(Start)*self.eval(End) < 0:
			if Flag:
				Start *= 10
				Flag = 0
			else:
				End *= -10
				Flag = 1
		return Start,End


	def Get_root_Newton(self, x = None, deghat = 0.00001):
		if x == None:
			x = random.randrange(1,10)
			while self.eval(x) == 0:
				x = random.randrange(0,10)
		self.newton_steps.append(x)
		if abs(self.eval(x)) < deghat:
			return x
		return self.Get_root_Newton(x - (self.eval(x) / self.derive().eval(x)))

	def draw_noghtechin(self,t, x2, y2):
		x1 , y1 = t.pos()
		step = 0.1 * scale
		counter = 0
		flag = 0
		if x1 != x2:
			flag = 1
			m = (y2-y1)/(x2-x1)
		if flag:
			while abs(x1 -x2) > 1:
				counter += 1
				if x1 < x2:
					x1 += step
					y1 += m * step
				else:
					x1 -= step
					y1 -= m * step
				if counter%3:
					t.penup()
					t.goto(x1, y1)
					t.pendown()
				else:
					t.goto(x1, y1)
				print flag, counter, x1, x2
		else:
			while abs(y1 -y2) > 1:
				counter += 1
				if y1 < y2:
					y1 += step
				else:
					y1 -= step
				if counter%3:
					t.penup()
					t.goto(x1, y1)
					t.pendown()
				else:
					t.goto(x1, y1)
				print flag, counter, y1, y2

			
	def draw_newton(self):
		newton_turtle = turtle.Turtle()
		newton_turtle.penup()
		newton_turtle.setpos(self.newton_steps[0]*scale,self.eval(self.newton_steps[0])*scale)
		newton_turtle.pendown()
		newton_turtle.color('blue')
		for i in range(1,len(self.newton_steps)):
			self.draw_noghtechin(newton_turtle, self.newton_steps[i]*scale,0)
			self.draw_line([self.newton_steps[i-1]*scale, self.eval(self.newton_steps[i-1]*scale)],
					[self.newton_steps[i]*scale, self.eval(self.newton_steps[i])*scale])
		raw_input("dfds")
		del(newton_turtle)			

	def Get_root_Bisection(self, deghat = 0.00001):
		Start ,End = self.find_dis()
		midIndex = (Start + End) / 2.0

		self.bisection_steps.append([Start,End])#set bisection steps in bisections_steps list 

		while abs(self.eval(midIndex)) > deghat:
			#print "haaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",Start,midIndex,End
			if self.eval(Start) == 0:
				return Start
			elif self.eval(End) == 0:
				return End
			if self.eval(midIndex) * self.eval(Start) < 0:
				End = midIndex
			elif self.eval(midIndex) * self.eval(End) < 0:
				Start = midIndex
			else:
				return midIndex
			midIndex = (Start + End)/2.0
			self.bisection_steps.append([Start,End])
			#print "hhhhhhhhhhhhhhhh",Start,End,midIndex
		return midIndex
	
	def draw_bisection(self):
		start_turtle=turtle.Turtle()
		start_turtle.penup()
		start_turtle.setheading(90)

	
		end_turtle=turtle.Turtle()
		end_turtle.penup()
		end_turtle.setheading(90)

		middle_turtle=turtle.Turtle()
		middle_turtle.penup()
		middle_turtle.pencolor('#32c181')
		dis=-(scale/4)
		for i in self.bisection_steps:
			start_turtle.setpos(i[0]*scale,0)
			end_turtle.setpos(i[1]*scale,0)

			middle_turtle.penup()
			dis -=10
			middle_turtle.setpos(i[0]*scale,dis)
			middle_turtle.pendown()
			middle_turtle.goto(i[1]*scale,dis)
			
			
			print 'bisection algorithm|','min:',i[0],'max:',i[1]
			self.delay()
		print "\nThe result is : ",self.bisection_steps[-1][0]

	def Get_root_dislocation(self, Start=None, End=None, deghat=0.00001):
		
		if not (Start and End):
			Start, End = self.find_dis()
#			self.dislocation_steps.append([(Start,self.eval(Start)),(End,self.eval(End))])
		x = (Start * self.eval(End) - End * self.eval(Start))/(self.eval(End)-self.eval(Start))
#		print "nabejayi|","start:",Start , self.eval(Start),"|",x,"|",End, self.eval(End)
		self.dislocation_steps.append([(Start,self.eval(Start)),(End,self.eval(End))])
		
		if abs(self.eval(x)) < deghat:
			return x
		else:
			if self.eval(Start) == 0:
				return Start
			elif self.eval(End) == 0:
				return End
			if self.eval(Start)*self.eval(x) < 0:
				return self.Get_root_dislocation(Start, x)
			else:
				return self.Get_root_dislocation(x, End)

	def draw_dislocation(self):
		
		'''its not work '''
		for pos in self.dislocation_steps:
			print pos
			self.draw_line(pos[0],pos[1])

		while 1:
			pass

	def Get_root_tendinous(self, deghat =0.00001):
		x0, x1 = -3,6
		self.tendinous_steps.append([x0,x1])
		while abs(self.eval(x1)) > deghat:
			x2 = ( x0*self.eval(x1)-x1*self.eval(x0))/(self.eval(x1)-self.eval(x0))
			x0, x1 = x1, x2
			self.tendinous_steps.append([x0,x1])
		return x1

	def draw_tendinous(self):
		print self.tendinous_steps
		for i in self.tendinous_steps:
			print i
			self.draw_line([i[0], self.eval(i[0])], [i[1], self.eval(i[1])])
			raw_input("")
		

class Cord(object):
	def __init__(self,direction,origin=(0,0),range=(-300,300),width=2,color=(0,0,0),):
		'''
			origin do not work
		'''
		self.direction=direction
		self.origin=origin
		
		self.range=range
		self.pos_turtle=turtle.Turtle()
                self.neg_turtle=turtle.Turtle()
		self.pos_turtle.speed(1)
		self.neg_turtle.speed(1)
		self.pos_turtle.setpos(origin)
		self.neg_turtle.setpos(origin)
		self.pos_turtle.color(color)
		self.neg_turtle.color(color)
		self.pos_turtle.width(width)
		self.neg_turtle.width(width)
		self.draw()

	def draw(self):
		if self.direction=="h":
			self.pos_turtle.goto(self.range[1],0)
			self.neg_turtle.goto(self.range[0],0)
		if self.direction=="v":
			self.pos_turtle.seth(90)
			self.pos_turtle.goto(0,self.range[1])
			self.neg_turtle.goto(0,self.range[0])

		
		self.neg_turtle.ht()
	def __del__(self):
		del(self.pos_turtle)
		del(self.neg_turtle)






def draw_rule(speed=0,width=1,color='#32c18f'):

	rule_turtle=turtle.Turtle()
	rule_turtle.speed(speed)
	rule_turtle.width(width)
	rule_turtle.pencolor(color)

	for i in range(-45,45):
		rule_turtle.penup()
		rule_turtle.setpos(i*scale,700)
		rule_turtle.pendown()
		rule_turtle.goto(i*scale,-700)

	for i in range(-30,30):
		rule_turtle.penup()
		rule_turtle.setpos(700,i*scale)
		rule_turtle.pendown()
		rule_turtle.goto(-700,i*scale)
	
	del(rule_turtle)

#def eval(x,confficients):
#	return confficients[0]*(x**2.0)+confficients[1]*x*1.0+confficients[2]*1.0
#	confficients[0]*(x**2.0)+confficients[1]*x*1.0+confficients[2]


def draw_scene():
	ts=turtle.getscreen()
	ts.title(page_title)
	ts.bgcolor(bg_color)

	draw_rule()
	x=Cord("v")
	y=Cord("h")

if __name__ == "__main__":
	draw_scene()
#	draw_curve()
#	raw_input("haha")
	#equ = Equation([1,-2,-1,4])
	equ=Equation(coefficients)
	equ.draw()
	print "equation is ",equ
	print "equ.eval(0)"
	print equ.eval(0)
	print "equ.derive().eval(0)"
	print "equ.derive is ",equ.derive()
	print equ.derive().eval(0)
	print "equ.Get_root_Newton()"
	print equ.Get_root_Newton()
	print equ.newton_steps
#	equ.draw_newton()
	print "equ.Get_root_Bisection()"
	print equ.Get_root_Bisection()

#	equ.draw_bisection()
	print "equ.Get_root_dislocation()"
	print equ.Get_root_dislocation()
	#equ.draw_dislocation()
	print "equ.Get_root_tendinous()"
	print equ.Get_root_tendinous()
	equ.draw_tendinous()
