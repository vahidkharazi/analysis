#!/usr/bin/python
from Tkinter import *
from scene import *

class FullScreenApp(object):
    def __init__(self, master, **kwargs):
        self.master=master
        pad=3
        self._geom='200x200+0+0'
        master.geometry("{0}x{1}+0+0".format(
            master.winfo_screenwidth()-pad, master.winfo_screenheight()-pad))
        master.bind('<Escape>',self.toggle_geom)            

    def toggle_geom(self,event):
        geom=self.master.winfo_geometry()
        print(geom,self._geom)
        self.master.geometry(self._geom)
        self._geom=geom


class Main(Frame):
	'''class that implement the graphical work of Analysis Field'''
	def __init__(self):
		'''File:[Equation,Exit],State:[Start,Pause,Again,Stop],Help:[Guidence]'''
		root = Tk()
		root.overrideredirect(True)
		root.geometry("{0}x{1}+0+0".format(root.winfo_screenwidth(),
                                       root.winfo_screenheight()))
		root.bind('<Escape>', self.toggle_geom)
		Frame.__init__(self, root)

		self.equ = Equation([0,1,0])		# must be the equation object for drawing
		self.drawFlag = 0			# that is -1 before equation assignment

		self.pack(expand = YES, fill = BOTH)
		self.master.title("Graphical Equation Solving")
#		self.master.geometry("200x200")
		
		self.MenuBar = Menu(self)

		menu = Menu(self.MenuBar, tearoff = 0)
		self.MenuBar.add_cascade(label = "File", menu = menu)
#		menu.add_command(label = "Equation",command = self.getEquation)
		menu.add_command(label = "Exit",command = exit)

#		menu = Menu(self.MenuBar, tearoff = 0)
#		self.MenuBar.add_cascade(label = "Run", menu = menu)
#		self.MenuBar.add_cascade(label = "Run",state ="disabled", menu = menu)
#		menu.add_command(label = "Start",command = self.startDraw)
#		menu.add_command(label = "Pause",command = self.pauseDraw)
#		menu.add_command(label = "Restart",command = self.restartDraw)
#		menu.add_command(label = "Stop",command = self.stopDraw)

#		menu = Menu(self.MenuBar, tearoff = 0)
#		self.MenuBar.add_cascade(label = "Help",menu = menu)
#		menu.add_command(label = "Guide to Graphical Equation Solving",command = self.guide)
		
		self.master.config(menu = self.MenuBar)

		self.entry0 = Entry(self, text = "")
		self.entry0.pack(side = BOTTOM, padx = 2, pady = 2)

		self.entry1 = Entry(self, text = "")
		self.entry1.pack(side = BOTTOM, padx = 2, pady = 2)

		self.entry2 = Entry(self, text = "")
		self.entry2.pack(side = BOTTOM, padx = 2, pady = 2)

		self.drawButton = Button(self, text = "Draw", command = self.Draw)
		self.drawButton.pack(side = BOTTOM)

		self.bisectionButton = Button(self, text = "Bisection", command = self.DrawBisection)
		self.bisectionButton.pack(side = BOTTOM, fill = "x")
		self.newtonButton = Button(self, text = "Newton", command = self.DrawNewton)
		self.newtonButton.pack(side = BOTTOM, fill = "x")
		self.dislocationButton = Button(self, text = "Dislocation", command = self.DrawDislocation)
		self.dislocationButton.pack(side = BOTTOM, fill = "x")

		self.tendinousButton = Button(self, text = "Tendinous", command = self.DrawVatari)
		self.tendinousButton.pack(side = BOTTOM, fill = "x")

	def DrawBisection(self):
		if not self.drawFlag:
			self.Draw()
		self.equ.Get_root_Bisection()
		self.equ.draw_bisection()

	def DrawNewton(self):
		if not self.drawFlag:
			self.Draw()
		self.equ.Get_root_Newton()
		self.equ.draw_newton()

	def DrawDislocation(self):
		if not self.drawFlag:
			self.Draw()
		self.equ.Get_root_dislocation()
		self.equ.draw_dislocation()

	def DrawVatari(self):
		if not self.drawFlag:
			self.Draw()
		self.equ.Get_root_tendinous()
		self.equ.draw_tendinous()

	def Draw(self):
		self.drawFlag = 1
		param = []
		param.append(float(self.entry2.get()))
		param.append(float(self.entry1.get()))
		param.append(float(self.entry0.get()))
		self.equ = Equation(param)
		draw_scene()
		self.equ.draw()

	def guide(self):
		print "guidence"
		pass	
		
def main():
	root = Tk(Main())
	app = FullScreenApp(root)
	root.mainloop()

if __name__ == "__main__":
	main()
	
	
